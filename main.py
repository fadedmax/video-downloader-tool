import os
import pyperclip

def makeUrl(url):
    downloadstring = f'youtube-dl "ytsearch:{url}"'
    return downloadstring


while True:
    url = input("URL: ")
    finalurl = makeUrl(url)
    print(finalurl)
    pyperclip.copy(finalurl)

